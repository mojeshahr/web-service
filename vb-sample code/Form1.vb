﻿Public Class Form1
 
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        txtSendResult.Text = ""

        'ایجاد شی ارتباط با وب سرویس
        Dim SMS_Service As New ir.sms_webservice.SmsV1

        'ایجاد آرایه شماره های گیرنده و قرار دادن شماره در آن 
        Dim RecipientNumbers() As String = {txtRecipientNumbers.Text}

        'آرایه مربوط به نتایج ارسال
        Dim result() As Long

        'ارسال مقادیر به وبسرویس
        result = SMS_Service.SendMessage(txtUsername.Text, txtPassWord.Text, txtMessageBodie.Text, RecipientNumbers, txtSenderNumber.Text, txtType.Text, txtAllowedDelay.Text)

        'چون در آرایه مربوط به گیرندگان فقط 1 شماره وجود داشت پس نتیجه ارسال در خانه صفر آرایه نتیجه برگردانده می شود
        If result(0) > 0 Then
            txtSendResult.Text = "SMS ID: " & result(0)
        Else
            'عدد منفی نشان دهنده کد خطاست
            'توضیحات کد خطا در فایل توضیحات
            txtSendResult.Text = "Error: " & result(0)
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        txtStatusResult.Text = ""

        'ایجاد شی ارتباط با وب سرویس
        Dim SMS_Service As New ir.sms_webservice.SmsV1

        'ایجاد آرایه شناسه پیامک ها و درج کد نوشته شده در آن 
        Dim MessageID() As Long = {txtMessageId.Text}

        'آرایه مربوط به نتایج ارسال
        Dim result() As Long

        'ارسال مقادیر به وبسرویس
        result = SMS_Service.GetMessagesStatus(txtUsername.Text, txtPassWord.Text, MessageID)

        'چون در آرایه مربوط به شناسه ها فقط 1 شناسه وجود داشت پس نتیجه ارسال در خانه صفر آرایه نتیجه برگردانده می شود
        If result(0) > 0 Then
            'عدد منفی نشان دهنده کد خطاست
            txtStatusResult.Text = "Status" & result(0)
        Else
            'عدد منفی نشان دهنده کد خطاست
            'توضیحات کد خطا در فایل توضیحات
            txtStatusResult.Text = "Error: " & result(0)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'ایجاد شی ارتباط با وب سرویس
        Dim SMS_Service As New ir.sms_webservice.SmsV1

        'ارسال مقادیر به وبسرویس
        txtCredit.Text = SMS_Service.GeCredit(txtUsername.Text, txtPassWord.Text)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'ایجاد شی ارتباط با وب سرویس
        Dim SMS_Service As New ir.sms_webservice.SmsV1

        'اگر خطایی رخ دهد مقدار آن در این متغیر ذخیره می شود
        Dim ErrNum As Integer


        Dim InboxSMS As String()()
        InboxSMS = SMS_Service.GetAllMessages(txtUsername.Text, txtPassWord.Text, 50, "", ErrNum)
        If Not ErrNum < 0 Then

            If Not InboxSMS Is Nothing Then
                For Each SMSItem As String() In InboxSMS
                    dgInbox.Rows.Add(SMSItem(0), SMSItem(1), SMSItem(2), SMSItem(3), SMSItem(4))
                 Next
            End If
        End If

    End Sub
End Class
