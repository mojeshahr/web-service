﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.txtPassWord = New System.Windows.Forms.TextBox
        Me.txtSenderNumber = New System.Windows.Forms.TextBox
        Me.txtMessageBodie = New System.Windows.Forms.TextBox
        Me.txtRecipientNumbers = New System.Windows.Forms.TextBox
        Me.txtType = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtSendResult = New System.Windows.Forms.TextBox
        Me.txtAllowedDelay = New System.Windows.Forms.TextBox
        Me.txtMessageId = New System.Windows.Forms.TextBox
        Me.txtStatusResult = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.Button3 = New System.Windows.Forms.Button
        Me.dgInbox = New System.Windows.Forms.DataGridView
        Me.Button4 = New System.Windows.Forms.Button
        Me.txtCredit = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.dgInbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(335, 173)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.txtAllowedDelay)
        Me.TabPage1.Controls.Add(Me.txtSendResult)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.txtType)
        Me.TabPage1.Controls.Add(Me.txtRecipientNumbers)
        Me.TabPage1.Controls.Add(Me.txtMessageBodie)
        Me.TabPage1.Controls.Add(Me.txtSenderNumber)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(327, 147)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "ارسال پیام"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.txtStatusResult)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.txtMessageId)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(327, 147)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "دریافت وضعیت"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPassWord)
        Me.GroupBox1.Controls.Add(Me.txtUsername)
        Me.GroupBox1.Location = New System.Drawing.Point(358, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(187, 147)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Login"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(17, 42)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(164, 21)
        Me.txtUsername.TabIndex = 1
        Me.txtUsername.Text = "username"
        '
        'txtPassWord
        '
        Me.txtPassWord.Location = New System.Drawing.Point(17, 83)
        Me.txtPassWord.Name = "txtPassWord"
        Me.txtPassWord.Size = New System.Drawing.Size(164, 21)
        Me.txtPassWord.TabIndex = 2
        Me.txtPassWord.Text = "password"
        '
        'txtSenderNumber
        '
        Me.txtSenderNumber.Location = New System.Drawing.Point(106, 13)
        Me.txtSenderNumber.Name = "txtSenderNumber"
        Me.txtSenderNumber.Size = New System.Drawing.Size(96, 21)
        Me.txtSenderNumber.TabIndex = 2
        Me.txtSenderNumber.Text = "30007546156"
        '
        'txtMessageBodie
        '
        Me.txtMessageBodie.Location = New System.Drawing.Point(106, 66)
        Me.txtMessageBodie.Name = "txtMessageBodie"
        Me.txtMessageBodie.Size = New System.Drawing.Size(96, 21)
        Me.txtMessageBodie.TabIndex = 3
        Me.txtMessageBodie.Text = "Hello !"
        '
        'txtRecipientNumbers
        '
        Me.txtRecipientNumbers.Location = New System.Drawing.Point(106, 39)
        Me.txtRecipientNumbers.Name = "txtRecipientNumbers"
        Me.txtRecipientNumbers.Size = New System.Drawing.Size(96, 21)
        Me.txtRecipientNumbers.TabIndex = 4
        Me.txtRecipientNumbers.Text = "09111111111"
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(106, 92)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(96, 21)
        Me.txtType.TabIndex = 5
        Me.txtType.Text = "1"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Button1.Location = New System.Drawing.Point(224, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 100)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "fill all field and click here to send"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtSendResult
        '
        Me.txtSendResult.Location = New System.Drawing.Point(208, 119)
        Me.txtSendResult.Name = "txtSendResult"
        Me.txtSendResult.ReadOnly = True
        Me.txtSendResult.Size = New System.Drawing.Size(113, 21)
        Me.txtSendResult.TabIndex = 7
        '
        'txtAllowedDelay
        '
        Me.txtAllowedDelay.Location = New System.Drawing.Point(106, 118)
        Me.txtAllowedDelay.Name = "txtAllowedDelay"
        Me.txtAllowedDelay.Size = New System.Drawing.Size(96, 21)
        Me.txtAllowedDelay.TabIndex = 8
        Me.txtAllowedDelay.Text = "5"
        '
        'txtMessageId
        '
        Me.txtMessageId.Location = New System.Drawing.Point(76, 23)
        Me.txtMessageId.Name = "txtMessageId"
        Me.txtMessageId.Size = New System.Drawing.Size(98, 21)
        Me.txtMessageId.TabIndex = 3
        Me.txtMessageId.Text = "19649654"
        '
        'txtStatusResult
        '
        Me.txtStatusResult.Location = New System.Drawing.Point(197, 118)
        Me.txtStatusResult.Name = "txtStatusResult"
        Me.txtStatusResult.ReadOnly = True
        Me.txtStatusResult.Size = New System.Drawing.Size(116, 21)
        Me.txtStatusResult.TabIndex = 9
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Button2.Location = New System.Drawing.Point(216, 13)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 100)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "fill all field and click here to Get Status" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.dgInbox)
        Me.TabPage3.Controls.Add(Me.Button3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(327, 147)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "دریافت پیامک"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.txtCredit)
        Me.TabPage4.Controls.Add(Me.Button4)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(327, 147)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "دریافت اعتبار"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Button3.Location = New System.Drawing.Point(18, 119)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(282, 24)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "Get Top 20 Message From inbox"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'dgInbox
        '
        Me.dgInbox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgInbox.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column2, Me.Column5, Me.Column4})
        Me.dgInbox.Location = New System.Drawing.Point(3, 8)
        Me.dgInbox.Name = "dgInbox"
        Me.dgInbox.Size = New System.Drawing.Size(321, 105)
        Me.dgInbox.TabIndex = 31
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Button4.Location = New System.Drawing.Point(221, 20)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 100)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "Get User SMS Credit"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'txtCredit
        '
        Me.txtCredit.Location = New System.Drawing.Point(47, 61)
        Me.txtCredit.Name = "txtCredit"
        Me.txtCredit.ReadOnly = True
        Me.txtCredit.Size = New System.Drawing.Size(116, 21)
        Me.txtCredit.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 200)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(530, 41)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "برای اضافه کردن وب سرویس به نرم افزار خود باید از منوی Project گزینه  Add Web Ref" & _
            "erence  آدرس وب سرویس نوشته شده در فایل توضیحات را انتخاب کنید"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Sender Number"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Recipient Number"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Text"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Message Type"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 121)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Allowed Delay"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Message ID"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label8.Location = New System.Drawing.Point(9, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label8.Size = New System.Drawing.Size(165, 56)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "کد پیامک همان مقدار برگردانده شده از تابع ارسال است"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Column1
        '
        Me.Column1.HeaderText = "ID"
        Me.Column1.Name = "Column1"
        '
        'Column3
        '
        Me.Column3.HeaderText = "To"
        Me.Column3.Name = "Column3"
        '
        'Column2
        '
        Me.Column2.HeaderText = "From"
        Me.Column2.Name = "Column2"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Text"
        Me.Column5.Name = "Column5"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Date"
        Me.Column4.Name = "Column4"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(557, 270)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.dgInbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents txtRecipientNumbers As System.Windows.Forms.TextBox
    Friend WithEvents txtMessageBodie As System.Windows.Forms.TextBox
    Friend WithEvents txtSenderNumber As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPassWord As System.Windows.Forms.TextBox
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtSendResult As System.Windows.Forms.TextBox
    Friend WithEvents txtAllowedDelay As System.Windows.Forms.TextBox
    Friend WithEvents txtStatusResult As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtMessageId As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents dgInbox As System.Windows.Forms.DataGridView
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents txtCredit As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
