﻿namespace webservice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnGetMessagesStatus = new System.Windows.Forms.Button();
            this.btnGetAllMessages = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtSenderNumber = new System.Windows.Forms.TextBox();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.txtMsgText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Rcepien = new System.Windows.Forms.Label();
            this.cboTypeofMsg = new System.Windows.Forms.ComboBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMsgId = new System.Windows.Forms.TextBox();
            this.lblMsgStatuse = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgMessage = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.lblgetCredit = new System.Windows.Forms.Label();
            this.btngetCredit = new System.Windows.Forms.Button();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMessage)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(486, 258);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 62);
            this.button1.TabIndex = 0;
            this.button1.Text = "sendMessage";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.sendMessage_Click);
            // 
            // btnGetMessagesStatus
            // 
            this.btnGetMessagesStatus.Location = new System.Drawing.Point(419, 82);
            this.btnGetMessagesStatus.Name = "btnGetMessagesStatus";
            this.btnGetMessagesStatus.Size = new System.Drawing.Size(129, 61);
            this.btnGetMessagesStatus.TabIndex = 1;
            this.btnGetMessagesStatus.Text = "GetMessagesStatus";
            this.btnGetMessagesStatus.UseVisualStyleBackColor = true;
            this.btnGetMessagesStatus.Click += new System.EventHandler(this.btnGetMessagesStatus_Click);
            // 
            // btnGetAllMessages
            // 
            this.btnGetAllMessages.Location = new System.Drawing.Point(658, 253);
            this.btnGetAllMessages.Name = "btnGetAllMessages";
            this.btnGetAllMessages.Size = new System.Drawing.Size(129, 54);
            this.btnGetAllMessages.TabIndex = 2;
            this.btnGetAllMessages.Text = "GetAllMessages";
            this.btnGetAllMessages.UseVisualStyleBackColor = true;
            this.btnGetAllMessages.Click += new System.EventHandler(this.btnGetAllMessages_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(460, 28);
            this.txtNumber.Multiline = true;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNumber.Size = new System.Drawing.Size(172, 197);
            this.txtNumber.TabIndex = 1;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(79, 26);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(134, 20);
            this.txtUserName.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(342, 26);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(134, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // txtSenderNumber
            // 
            this.txtSenderNumber.Location = new System.Drawing.Point(113, 34);
            this.txtSenderNumber.Name = "txtSenderNumber";
            this.txtSenderNumber.Size = new System.Drawing.Size(134, 20);
            this.txtSenderNumber.TabIndex = 5;
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(114, 129);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(134, 20);
            this.txtDelay.TabIndex = 7;
            // 
            // txtMsgText
            // 
            this.txtMsgText.Location = new System.Drawing.Point(107, 173);
            this.txtMsgText.Multiline = true;
            this.txtMsgText.Name = "txtMsgText";
            this.txtMsgText.Size = new System.Drawing.Size(253, 165);
            this.txtMsgText.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "UserName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(283, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "SenderNamber";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "TypeOfMessage";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Delay";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "MessageText";
            // 
            // Rcepien
            // 
            this.Rcepien.AutoSize = true;
            this.Rcepien.Location = new System.Drawing.Point(340, 48);
            this.Rcepien.Name = "Rcepien";
            this.Rcepien.Size = new System.Drawing.Size(94, 13);
            this.Rcepien.TabIndex = 17;
            this.Rcepien.Text = "RecipientNumbers";
            // 
            // cboTypeofMsg
            // 
            this.cboTypeofMsg.FormattingEnabled = true;
            this.cboTypeofMsg.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.cboTypeofMsg.Location = new System.Drawing.Point(114, 79);
            this.cboTypeofMsg.Name = "cboTypeofMsg";
            this.cboTypeofMsg.Size = new System.Drawing.Size(134, 21);
            this.cboTypeofMsg.TabIndex = 18;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(540, 307);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 19;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 67);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(840, 381);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage1.Controls.Add(this.txtNumber);
            this.tabPage1.Controls.Add(this.lblResult);
            this.tabPage1.Controls.Add(this.Rcepien);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.cboTypeofMsg);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtMsgText);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.txtDelay);
            this.tabPage1.Controls.Add(this.txtSenderNumber);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(832, 355);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Send SMS";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtMsgId);
            this.tabPage2.Controls.Add(this.lblMsgStatuse);
            this.tabPage2.Controls.Add(this.btnGetMessagesStatus);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(832, 355);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Get SMS Statuse";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "MessageId";
            // 
            // txtMsgId
            // 
            this.txtMsgId.Location = new System.Drawing.Point(97, 44);
            this.txtMsgId.Multiline = true;
            this.txtMsgId.Name = "txtMsgId";
            this.txtMsgId.Size = new System.Drawing.Size(185, 123);
            this.txtMsgId.TabIndex = 3;
            // 
            // lblMsgStatuse
            // 
            this.lblMsgStatuse.AutoSize = true;
            this.lblMsgStatuse.Location = new System.Drawing.Point(40, 213);
            this.lblMsgStatuse.Name = "lblMsgStatuse";
            this.lblMsgStatuse.Size = new System.Drawing.Size(0, 13);
            this.lblMsgStatuse.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage3.Controls.Add(this.dgMessage);
            this.tabPage3.Controls.Add(this.btnGetAllMessages);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(832, 355);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Get All SMS";
            // 
            // dgMessage
            // 
            this.dgMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMessage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgMessage.Location = new System.Drawing.Point(29, 35);
            this.dgMessage.Name = "dgMessage";
            this.dgMessage.Size = new System.Drawing.Size(540, 299);
            this.dgMessage.TabIndex = 3;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.lblgetCredit);
            this.tabPage4.Controls.Add(this.btngetCredit);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(832, 355);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Get Credit";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "SMS Count";
            // 
            // lblgetCredit
            // 
            this.lblgetCredit.AutoSize = true;
            this.lblgetCredit.Location = new System.Drawing.Point(98, 50);
            this.lblgetCredit.Name = "lblgetCredit";
            this.lblgetCredit.Size = new System.Drawing.Size(0, 13);
            this.lblgetCredit.TabIndex = 1;
            // 
            // btngetCredit
            // 
            this.btngetCredit.Location = new System.Drawing.Point(585, 236);
            this.btngetCredit.Name = "btngetCredit";
            this.btngetCredit.Size = new System.Drawing.Size(168, 66);
            this.btngetCredit.TabIndex = 0;
            this.btngetCredit.Text = "getCredit";
            this.btngetCredit.UseVisualStyleBackColor = true;
            this.btngetCredit.Click += new System.EventHandler(this.btngetCredit_Click);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "MessageId";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Receiver Number";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Sender Number";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Message Text";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Message Get Time";
            this.Column5.Name = "Column5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 460);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Name = "Form1";
            this.Text = "Payam-resan.com SMS Sender WebService";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMessage)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnGetMessagesStatus;
        private System.Windows.Forms.Button btnGetAllMessages;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtSenderNumber;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.TextBox txtMsgText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Rcepien;
        private System.Windows.Forms.ComboBox cboTypeofMsg;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgMessage;
        private System.Windows.Forms.Label lblMsgStatuse;
        private System.Windows.Forms.Button btngetCredit;
        private System.Windows.Forms.Label lblgetCredit;
        private System.Windows.Forms.TextBox txtMsgId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}

