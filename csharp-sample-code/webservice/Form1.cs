﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using webservice.webservice;


namespace webservice
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// send Message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendMessage_Click(object sender, EventArgs e)
        {
            SmsV1 smsvebservice = new SmsV1();
            string[] RecipientNumbers = new string[100];
            RecipientNumbers = txtNumber.Text.Split(',').ToArray();
            long[] Result = new long[100];
            int type = int.Parse(cboTypeofMsg.SelectedItem.ToString());
            Result = smsvebservice.SendMessage(txtUserName.Text, txtPassword.Text, txtMsgText.Text, RecipientNumbers, txtSenderNumber.Text, type, int.Parse(txtDelay.Text));

            if (Result[0] > 0)
            {
                lblResult.Text = "SMS ID: " + Result[0];
            }
            else {
                
                lblResult.Text = "Error: " + Result[0];
            }
        }
        /// <summary>
        /// GetMessagesStatus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetMessagesStatus_Click(object sender, EventArgs e)
        {

            SmsV1 smsvebservice = new SmsV1();
            string[] strmsg=(txtMsgId.Text.Split(',').ToArray());
            long[] MessageId = new long[strmsg.Length];
            for (int i = 0; i < strmsg.Length; i++)
            {
                MessageId[i] =Convert.ToInt64(strmsg[i]);
            }
            long[] Result = new long[100];
            Result = smsvebservice.GetMessagesStatus(txtUserName.Text, txtPassword.Text, MessageId);
            if (Result[0] > 0)
            {
                for (int i = 0; i < Result.Length; i++)
                {
                    lblMsgStatuse.Text += "Status of "+MessageId[i] + " : "+Result[i]+"\n";
                }
                
            }
            else {
                lblMsgStatuse.Text = "Error: " + Result[0];
            }
               
        }

        /// <summary>
        /// Get AllMessage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetAllMessages_Click(object sender, EventArgs e)
        {
            SmsV1 smsvebservice = new SmsV1();
           int ErroreNum = 0;
           //List<string[]> InboxMsg = new List<string[]>();
           string[][] Inbox = new string[100][];
           Inbox = smsvebservice.GetAllMessages(txtUserName.Text, txtPassword.Text, 20, "", ref ErroreNum);

            
           if (!(ErroreNum < 0))
           {
               if (Inbox.Length > 0) 
               {
                   int i=0;
                   foreach (string[] smsitem in Inbox) {
                       dgMessage.Rows.Add();
                       dgMessage.Rows[i].Cells[0].Value=smsitem[0];
                       dgMessage.Rows[i].Cells[1].Value = smsitem[1];
                       dgMessage.Rows[i].Cells[2].Value = smsitem[2];
                       dgMessage.Rows[i].Cells[3].Value = smsitem[3];
                       dgMessage.Rows[i].Cells[4].Value = smsitem[4];
                       i++;
                   }
                   
               }
           }
          
        }

        private void btngetCredit_Click(object sender, EventArgs e)
        {
            SmsV1 smsvebservice = new SmsV1();
            lblgetCredit.Text = smsvebservice.GeCredit(txtUserName.Text, txtPassword.Text).ToString();
        }

    }
}
